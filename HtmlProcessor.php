<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $text = $_POST['text'] ?? file_get_contents('php://input');
    if (empty($text)) {
        http_response_code(500);
        exit();
    }

    $formatted_text = preg_replace('/<a.*?>(.*?)<\/a>/', '$1', $text);


    
    $response = array('formatted_text' => $formatted_text);
    header('Content-Type: application/json');
    echo json_encode($response);
}
