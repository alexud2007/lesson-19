<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<h1>HTML импорт</h1>
<form method="post" action="./html_import_processor.php">
    <label for="url">URL-адрес:</label>
    <input type="text" id="url" name="url">
    <button type="submit">Импорт</button>
</form>

<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $url = $_POST['url'];
    if (empty($url)) {
        echo '<p>Введите URL-адрес.</p>';
    } else {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $raw_text = curl_exec($ch);
        curl_close($ch);


        $data = array('text' => $raw_text);
        $ch = curl_init('http://localhost/lesson-19/HtmlProcessor.php');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($http_code === 200) {
            $response = json_decode($result, true);
            echo '<h2>Отформатированный текст:</h2>';
            echo '<p>' . $response['formatted_text'] . '</p>';
        } else {
            echo '<p>Ошибка обработки текста.</p>';
        }
    }
}
?>
</body>
</html>